# Question 4
Trouble Shooting Go Routine program

## How this program works ?
1. Program ini merupakan salah satu penerapan go routine, dimana dapat menjalankan 2 atau lebih func secara bersamaan. 
2. Terdapat kesalahan pada program ini yaitu tidak mencetak angka 10. Angka 10 ini didapatkan dari variabel N dan digunakan sebagai ukuran m yang memiliki tipe data map. 
3. Sebelum dilakukan trouble shooting, program mencetak sebanyak i acak yang dimasukkan
Hal tersebut dikarenakan i pada perulangan for dimasukkan secara acak pada func. Sehingga terdapat kemungkinan i yag dimasukkan bernilai sama
4. Setelah dilakukan trouble shooting, program mencetak 10 
5. Hal tersebut dikarenakan setiap nilai i pada perulangan for digunakan pada func meskipun  pengambilan dilakukan secara acak