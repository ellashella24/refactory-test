package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"
)

type Inventory struct {
	InventoryID int64     `json:"inventory_id"`
	Name        string    `json:"name"`
	Type        string    `json:"type"`
	Tags        []string  `json:"tags"`
	PurchasedAt int64     `json:"purchased_at"`
	Placement   Placement `json:"placement"`
}

type Placement struct {
	RoomID string `json:"room_id"`
	Name   string `json:"name"`
}

func findItemInMeetingRoom(inventory []Inventory, str string) []Inventory {
	var result []Inventory

	for i := 0; i < len(inventory); i++ {
		if strings.Contains(inventory[i].Placement.Name, str) {
			result = append(result, inventory[i])
		}
	}

	return result
}

func findAllElectoronicDevices(inventory []Inventory, str string) []Inventory {
	var result []Inventory

	for i := 0; i < len(inventory); i++ {
		if strings.Contains(inventory[i].Type, str) {
			result = append(result, inventory[i])
		}
	}

	return result
}

func findAllFurniture(inventory []Inventory, str string) []Inventory {
	var result []Inventory

	for i := 0; i < len(inventory); i++ {
		if strings.Contains(inventory[i].Type, str) {
			result = append(result, inventory[i])
		}
	}

	return result
}

func findItemPurchasedAt(inventory []Inventory, purchasedAt time.Time) []Inventory {
	var result []Inventory

	for i := 0; i < len(inventory); i++ {
		PurchasedAt := time.Unix(inventory[i].PurchasedAt, 0)
		y1, m1, d1 := PurchasedAt.Date()
		y2, m2, d2 := purchasedAt.Date()
		if y1 == y2 && m1 == m2 && d1 == d2 {
			result = append(result, inventory[i])
		}
	}

	return result
}

func findAllItemBrownColor(inventory []Inventory, str string) []Inventory {
	var result []Inventory

	for i := 0; i < len(inventory); i++ {
		if strings.Contains(inventory[i].Name, str) {
			result = append(result, inventory[i])
		}
	}

	return result
}

func main() {
	jsonFile, err := os.Open("inventory_list.json")

	if err != nil {
		fmt.Println(err)
	}

	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var inventory []Inventory

	json.Unmarshal(byteValue, &inventory)

	fmt.Println("All items in the Meeting Room")
	fmt.Println(findItemInMeetingRoom(inventory, "Meeting Room"))

	fmt.Println("All electronic devices")
	fmt.Println(findAllElectoronicDevices(inventory, "electronic"))

	fmt.Println("All the furniture")
	fmt.Println(findAllFurniture(inventory, "furniture"))

	fmt.Println("All items were purchased on 16 Januari 2020")
	fmt.Println(findAllItemBrownColor(inventory, "Brown"))

	fmt.Println("All items were purchased on 16 Januari 2020")
	i, err := strconv.ParseInt("1579190471", 10, 64)
	date := time.Unix(i, 0)

	fmt.Println("All items with brown color")
	fmt.Println(findItemPurchasedAt(inventory, date))

}
