# Question 3
Program that contains several function that return a value based on the given task
---

### Task 1
`func findItemInMeetingRoom (inventory []Inventory, str string) []Inventory`
- Parameter masukan: `inventory []Inventory` merupakan data inventory, `str` merupakan nama ruangan yang dicari
- Return value: `[]Inventory` merupakan inventory pada nama ruang yang dicari

#### How this program works ?
1. Fungsi ini akan mengembalikan nilai berupa inventory pada ruangan
2. Fungsi ini akan mencocokan nama ruangan yang dimasukkan dengan nama ruangan pada file json menggunakan fungsi yang tersedia pada golang yaitu Contains
3. Jika keduanya cocok, maka program akan menambah inventory pada variabel result
4. Variabel result ini berisi inventory apa saja berada pada ruangan yang dimasukkan 
---

### Task 2
`func findAllElectoronicDevices (inventory []Inventory, str string) []Inventory`
- Parameter masukan: `inventory []Inventory` merupakan data inventory, `str` merupakan type inventory yang dicari
- Return value: `[]Inventory `merupakan inventory memiliki type yang sesuai dengan yang dicari

#### How this program works ?
1. Fungsi ini akan mengembalikan nilai berupa inventory pada type yang dicari
2. Fungsi ini akan mencocokan nama type yang dimasukkan dengan nama ruangan pada file json menggunakan fungsi yang tersedia pada golang yaitu Contains
3. Jika keduanya cocok, maka program akan menambah inventory pada variabel result
4. Variabel result ini berisi inventory apa saja berada pada type yang dimasukkan 
---

### Task 3
`findAllFurniture(inventory []Inventory, str string) []Inventory `
- Parameter masukan: inventory []Inventory merupakan data inventory, `str` merupakan type inventory yang dicari
- Return value: `[]Inventory` merupakan inventory memiliki type yang sesuai dengan type yang dicari

#### How this program works ?
1. Fungsi ini akan mengembalikan nilai berupa inventory pada type yang dicari
2. Fungsi ini akan mencocokan nama type yang dimasukkan dengan nama ruangan pada file json menggunakan fungsi yang tersedia pada golang yaitu `Contains`
3. Jika keduanya cocok, maka program akan menambah inventory pada variabel result
4. Variabel result ini berisi inventory apa saja berada pada type yang dimasukkan 
---

### Task 4
`findItemPurchasedAt(inventory []Inventory, purchasedAt time.Time) []Inventory`
- Parameter masukan: `inventory []Inventory` merupakan data inventory,` purchasedAt time.Time` merupakan tanggal pembelian inventory yang dicari
- Return value: `[]Inventory `merupakan inventory yang memiliki tanggal yang sesuai dengan tanggal pembelian inventory yang dicari

#### How this program works ?
1. Fungsi ini akan mengembalikan nilai berupa inventory pada tanggal yang dicari
2. Fungsi ini akan mencocokan tanggal yang dimasukkan dengan tanggal pembelian inventory pada file json 
3. Jika keduanya cocok, maka program akan menambah inventory pada variabel result
4. Variabel result ini berisi inventory apa saja berada pada tanggal pembelan inventory yang dicari
---

### Task 5
`findAllItemBrownColor(inventory []Inventory, str string) []Inventory`
- Parameter masukan: `inventory []Inventory` merupakan data inventory, `str `merupakan nama warna inventory - yang dicari
- Return value: `[]Inventory` merupakan inventory memiliki type yang sesuai dengan nama warna yang dicari

#### How this program works ?
1. Fungsi ini akan mengembalikan nilai berupa inventory pada warna yang dicari
2. Fungsi ini akan mencocokan nama warna yang dimasukkan dengan nama warna pada file json menggunakan fungsi yang tersedia pada golang yaitu Contains
3. Jika keduanya cocok, maka program akan menambah inventory pada variabel result
4. Variabel result ini berisi inventory apa saja berada pada nama warna yang dimasukkan 
---