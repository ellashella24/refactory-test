package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"text/tabwriter"
	"time"
)

func main() {
	var itemName []string
	var itemPrice []int64
	var priceTotal int64 = 0

	scanner := bufio.NewScanner(os.Stdin)

	fmt.Printf("Nama Warung: ")
	scanner.Scan()
	restoName := scanner.Text()
	fmt.Printf("Nama Kasir: ")
	scanner.Scan()
	cashierName := scanner.Text()
	fmt.Println("========================")

	for {
		fmt.Printf("Nama Menu: ")
		scanner.Scan()
		a := scanner.Text()
		itemName = append(itemName, a)
		fmt.Printf("Harga: ")
		scanner.Scan()
		b, _ := strconv.ParseInt(scanner.Text(), 10, 64)
		itemPrice = append(itemPrice, b)
		priceTotal += b
		fmt.Printf("Continue ? (Type anything to continue & type 'exit' to stop): ")
		scanner.Scan()
		c := scanner.Text()
		if c == "exit" || c == "EXIT" || c == "Exit" {
			break
		}
	}

	w := new(tabwriter.Writer)
	fmt.Printf("\n\n\n")
	fmt.Println(restoName)
	dt := time.Now()
	fmt.Println("Tanggal: ", dt.Format("02-01-2006 15:04:05"))
	w.Init(os.Stdout, 0, 8, 15, ' ', tabwriter.AlignRight)
	fmt.Printf("Nama Kasir:")
	fmt.Fprintf(w, "\t%s", cashierName)
	w.Flush()
	fmt.Printf("\n")
	fmt.Println("=============================")
	for i := 0; i < len(itemName); i++ {
		fmt.Printf("%s", itemName[i])
		var e = 0
		if len(itemName[i]) > 4 {
			e = 15
		} else {
			e = 16
		}
		w.Init(os.Stdout, 0, 8, e, '.', tabwriter.AlignRight)
		fmt.Fprintf(w, "\tRp.")
		w.Flush()
		var t = 0
		if len(strconv.Itoa(int(itemPrice[i]))) < 5 {
			t = 2
		} else {
			t = 1
		}

		w.Init(os.Stdout, 0, 2, t, ' ', tabwriter.AlignRight)
		fmt.Fprintf(w, "\t%d", itemPrice[i])
		w.Flush()
		fmt.Printf("\n")
	}
	fmt.Println("_____________________________")
	fmt.Printf("Total")
	var t = 0
	if len(strconv.Itoa(int(priceTotal))) < 5 {
		t = 16
	} else {
		t = 15
	}
	w.Init(os.Stdout, 0, 8, t, '.', tabwriter.AlignRight)
	fmt.Fprintf(w, "\tRp.")
	w.Flush()
	w.Init(os.Stdout, 0, 1, 1, ' ', tabwriter.AlignRight)
	fmt.Fprintf(w, "\t%d", priceTotal)
	w.Flush()
}
