package routes

import (
	"net/http"
	"question5/controllers"

	"github.com/labstack/echo/v4"
)

func Init() *echo.Echo {
	e := echo.New()

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello World !!!! ")
	})

	e.POST("/check-service", controllers.CheckService)

	return e
}
