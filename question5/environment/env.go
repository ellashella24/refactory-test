package environment

import "github.com/tkanos/gonfig"

type Environment struct {
	LOGS_DESTINATION string
}

func GetEnv() Environment {
	env := Environment{}
	gonfig.GetConf("environment/env.json", &env)
	return env
}
