module question5

go 1.15

require (
	github.com/canthefason/go-watcher v0.2.4 // indirect
	github.com/fatih/color v1.10.0 // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/labstack/echo/v4 v4.1.17
	github.com/tkanos/gonfig v0.0.0-20210106201359-53e13348de2f
	gopkg.in/fsnotify.v1 v1.4.7 // indirect
)
