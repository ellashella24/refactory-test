# Question 5
Client-server app that receive a POST request with payload

## How this program works ?
1. Server menerima HTTP request berupa post. Request yang diterima oleh server berupa data json dari header dan body
2. Data JSON diubah menjadi string dan diteruskan ke function logger
3. Function logger ini digunakan untuk menyimpan data JSON beserta tanggal dan waktu request dikirimkan serta http status
4. Hasil dari function logger kemudian disimpan pada file server.log
