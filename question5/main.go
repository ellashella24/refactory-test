package main

import "question5/routes"

func main() {
	e := routes.Init()

	e.Logger.Fatal(e.Start(":1234"))
}
