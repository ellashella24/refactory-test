package logger

import (
	"log"
	"os"
	"time"

	"question5/environment"

	"github.com/labstack/echo/v4"
)

func Success(c echo.Context, message string) {
	env := environment.GetEnv()
	layoutFormat := "2006-01-02T15:04:05"
	date := time.Now().Format(layoutFormat)
	method := c.Request().Method
	ip := c.RealIP()

	f, err := os.OpenFile(env.LOGS_DESTINATION,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()

	logger := log.New(f, "", 0)
	logger.Println("["+date+"]", "Success:", method, ip, message)

}
