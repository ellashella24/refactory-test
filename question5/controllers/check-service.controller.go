package controllers

import (
	"encoding/json"
	"question5/lib/logger"
	"strconv"

	"github.com/labstack/echo/v4"
)

type CheckServicePayload struct {
	XRandom string `json:"X-RANDOM"`
	Counter int64  `json:"counter"`
}

func CheckService(c echo.Context) error {
	counter, _ := strconv.Atoi(c.FormValue("counter"))

	payload := CheckServicePayload{
		XRandom: c.Request().Header.Get("X-RANDOM"),
		Counter: int64(counter),
	}

	payloadJSON, _ := json.Marshal(payload)

	logger.Success(c, string(payloadJSON))

	return c.JSON(201, true)

}
