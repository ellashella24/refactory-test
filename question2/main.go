package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"
)

type Identity struct {
	UserID   int64      `json:"id"`
	Username string     `json:"username"`
	Profile  Profile    `json:"profile"`
	Articles []Articles `json:"articles:"`
}

type Profile struct {
	Fullname string   `json:"full_name"`
	Birthday string   `json:"birthday"`
	Phones   []string `json:"phones"`
}

type Articles struct {
	ID          int64  `json:"id"`
	Title       string `json:"title"`
	PublishedAt string `json:"published_at"`
}

func findEmptyPhone(identity []Identity) []Identity {
	var result []Identity

	for i := 0; i < len(identity); i++ {
		if len(identity[i].Profile.Phones) == 0 {
			result = append(result, identity[i])
		}
	}

	return result
}

func findEmptyArticle(identity []Identity) []Identity {
	var result []Identity

	for i := 0; i < len(identity); i++ {
		if len(identity[i].Articles) == 0 {
			result = append(result, identity[i])
		}
	}

	return result
}

func findNameWithout(identity []Identity, str string) []Identity {
	var result []Identity
	for i := 0; i < len(identity); i++ {
		if !strings.Contains(identity[i].Profile.Fullname, str) {
			result = append(result, identity[i])
		}
	}

	return result
}

func findArticleOnYear(identity []Identity, year int) []Identity {
	var result []Identity
	for i := 0; i < len(identity); i++ {
		var flag int64 = 0
		for j := 0; j < len(identity[i].Articles); j++ {
			layoutFormat := "2006-01-02T15:04:05"
			publishedAt, _ := time.Parse(layoutFormat, identity[i].Articles[j].PublishedAt)
			if publishedAt.Year() == year {
				flag += 1
			}
		}

		if flag > 0 {
			result = append(result, identity[i])
		}
	}

	return result
}

func findBornOnYear(identity []Identity, year int) []Identity {
	var result []Identity
	for i := 0; i < len(identity); i++ {
		layoutFormat := "2006-01-02"
		born, _ := time.Parse(layoutFormat, identity[i].Profile.Birthday)
		if born.Year() == year {
			result = append(result, identity[i])
		}
	}

	return result
}

func findArticleTitle(identity []Identity, title string) []Articles {
	var result []Articles
	for i := 0; i < len(identity); i++ {
		for j := 0; j < len(identity[i].Articles); j++ {
			if strings.Contains(identity[i].Articles[j].Title, title) {
				result = append(result, identity[i].Articles[j])
			}
		}
	}

	return result
}

func findArticleOnPublishedAt(identity []Identity, publishedAt time.Time) []Articles {
	var result []Articles
	for i := 0; i < len(identity); i++ {
		for j := 0; j < len(identity[i].Articles); j++ {
			layoutFormat := "2006-01-02T15:04:05"
			PublishedAt, _ := time.Parse(layoutFormat, identity[i].Articles[j].PublishedAt)
			if PublishedAt.Before(publishedAt) {
				result = append(result, identity[i].Articles[j])
			}
		}
	}

	return result
}

func main() {
	jsonFile, err := os.Open("profile_list.json")

	if err != nil {
		fmt.Println(err)
	}

	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var identity []Identity

	json.Unmarshal(byteValue, &identity)

	fmt.Println("Users who don't have any phone numbers")
	fmt.Println(findEmptyPhone(identity))

	fmt.Println("Users who have articles")
	fmt.Println(findEmptyArticle(identity))

	fmt.Println("Users who have 'annis' on their name")
	fmt.Println(findNameWithout(identity, "Annis"))

	fmt.Println("Users who have articles on the year 2020")
	fmt.Println(findArticleOnYear(identity, 2020))

	fmt.Println("Users who are born in 1986")
	fmt.Println(findBornOnYear(identity, 1986))

	fmt.Println("Articles that contain 'tips' on the title")
	fmt.Println(findArticleTitle(identity, "Tips"))

	fmt.Println("Articles published before August 2019")
	date, _ := time.Parse("2006-01-02T15:04:05", "2019-08-01T01:00:00")
	fmt.Println(findArticleOnPublishedAt(identity, date))
}
