# Question 2
Program that contains several function that return a value based on the given task
---

### Task 1
`findEmptyPhone(identity []Identity) []Identity`
- Parameter masukan: `identity []Identity` merupakan data identity
- Return value: `[]Identity` merupakan nilai kembalian yang berupa array struct indentity yang berisi data identity yang tidak memiliki phones

#### How this program works ?
1. Fungsi ini melakukan pengecekan anggota identity yang tidak memiliki phone dengan cara menghitung ukuran array phones. 
2. Jika ukuran array sama dengan 0, maka anggota identity tersebut tidak memiliki phones
3. Jika menemukan anggota identity yang tidak memiliki phones, data anggota identity ditambahkan ke variabel result
---

### Task 2
`findEmptyArticle(identity []Identity) []Identity`
- Parameter masukan: `identity []Identity` merupakan data identity
- Return value: `[]Identity` merupakan nilai kembalian yang berupa array struct indentity yang berisi data identity yang tidak memiliki article

#### How this program works ?
1. Fungsi ini melakukan pengecekan anggota identity yang tidak memiliki phone dengan cara menghitung ukuran array article. 
2. Jika ukuran array article sama dengan 0, maka anggota identity tersebut tidak memiliki article
3. Jika menemukan anggota identity yang tidak memiliki article, data anggota identity ditambahkan ke variabel result
---

### Task 3
`findNameWithout(identity []Identity, str string) []Identity`
- Parameter masukan: identity []Identity merupakan data identity dan memiliki tipe data array struct Identity, str string merupakan tahun artikel yang dicari dan memiliki tipe data string
- Return value: []Identity merupakan nilai kembalian yang berupa array struct indentity yang berisi data identity yang tidak memiliki nama yang dimasukkan 

#### How this program works ?
1. Fungsi ini melakukan pengecekan anggota identity yang tidak memiliki nama yang sesuai dengan nama yang dimasukkan menggunakan fungsi yang tersedia pada golang yaitu Contains. 
2. Jika menemukan anggota identity sesuai, data anggota identity ditambahkan ke variabel result
---

### Task 4
`findArticleOnYear(identity []Identity, year int) []Identity `
- Parameter masukan: `identity []Identity` merupakan data identity dan memiliki tipe data array struct Identity, year merupakan tahun publikasi artikel yang dicari dan memiliki tipe data integer
- Return value: `[]Identity `merupakan nilai kembalian yang berupa array struct indentity yang berisi data identity yang memiliki tahun publikasi yang dimasukkan 

#### How this program works ?
1. Fungsi ini melakukan pengecekan anggota identity yang memiliki tahun yang sesuai dengan tahun publikasi artikel yang dimasukkan 
2. Jika menemukan anggota identity sesuai, data anggota identity ditambahkan ke variabel result
---

### Task 5
`findBornOnYear(identity []Identity, year int) []Identity`
- Parameter masukan: `identity []Identity` merupakan data identity dan memiliki tipe data array struct Identity, year merupakan tahun lahir user yang dicari dan memiliki tipe data integer 
- Return value: `[]Identity `merupakan nilai kembalian yang berupa array struct indentity yang berisi data identity yang memiliki tahun lahir user publikasi yang dimasukkan

#### How this program works ?
1. Fungsi ini melakukan pengecekan anggota identity yang memiliki tahun yang sesuai dengan tahun lahir user yang dimasukkan 
2. Jika menemukan anggota identity sesuai, data anggota identity ditambahkan ke variabel result
---

### Task 6
`findArticleTitle(identity []Identity, title string) []Articles`
- Parameter masukan: `identity []Identity` merupakan data identity dan memiliki tipe data array struct Identity, year merupakan judul artikel yang dicari dan memiliki tipe data string
- Return value: `[]Articles` merupakan nilai kembalian yang berupa array struct articles yang berisi data article yang memiliki judul artikel yang sesuai dengan input

#### How this program works ?
1. Fungsi ini melakukan pengecekan artikel pada setiap identity yang memiliki judul yang sesuai dengan judul yang dimasukkan menggunakan fungsi yang tersedia pada golang yaitu Contains. 
2. Jika menemukan anggota identity sesuai, data anggota identity ditambahkan ke variabel result
---

### Task 7
`findArticleOnPublishedAt(identity []Identity, publishedAt time.Time) []Articles`
- Parameter masukan: `identity []Identity` merupakan data identity dan memiliki tipe data array struct Identity, publishedAt merupakan tanggal publikasi dan memiliki tipe data time.Time
- Return value: `[]Articles` merupakan nilai kembalian yang berupa array struct articles yang berisi data article yang  memiliki tanggal publikasi sebelum yang sesuai dengan input

#### How this program works ?
1. Fungsi ini melakukan pengecekan anggota identity yang memiliki tahun publikasi sebelum tahun dimasukkan 
2. Jika menemukan anggota identity sesuai, data anggota identity ditambahkan ke variabel result
---
